# Puma Metrics Exporter

`Puma Metrics Exporter` is a simple Go server which gathers Puma metrics by using provided Puma control center URL and exports
them via `/metrics` HTTP endpoint for Prometheus scraper.

## Usage

In the Puma config of your Rails app, add the following line:

```ruby
# config/puma.rb
activate_control_app "tcp://127.0.0.1:9293", { auth_token: "foo" }
```

Alternatively, you can start your Puma server with the following options: `--control-url tcp://127.0.0.1:9293 --control-token foo`.

Finally, after starting your Puma server, run the following to get your exporter running
once you've build it:

``` shell
puma-metrics-exporter --puma-control-url "http://127.0.0.1:9293" --puma-control-auth-token "foo" --puma-clustered true
```
