package main

import (
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewServer(t *testing.T) {
	server, err := NewServer("127.0.0.1", 8080)

	require.Nil(t, err)
	require.NotNil(t, server)
	require.Equal(t, server.listen, "127.0.0.1")
	require.Equal(t, server.port, uint(8080))
}

func TestNewServerInvalidPortNumber(t *testing.T) {
	server, err := NewServer("127.0.0.1", 70000)

	require.EqualError(t, err, "invalid port value: 70000")
	require.Nil(t, server)
}

func TestListenAddress(t *testing.T) {
	server, err := NewServer("127.0.0.1", 9090)

	require.Nil(t, err)
	require.NotNil(t, server)

	listenAddress := server.listenAddress()

	require.Equal(t, listenAddress, "127.0.0.1:9090")
}

func TestIpAllowed(t *testing.T) {
	tests := []struct {
		name             string
		requestIP        string
		allowedIPNetwork string
		allowed          bool
	}{
		{
			name:             "Allowed IP",
			requestIP:        "10.0.0.8",
			allowedIPNetwork: "10.0.0.0/16",
			allowed:          true,
		},
		{
			name:             "Another allowed IP",
			requestIP:        "10.0.1.12",
			allowedIPNetwork: "10.0.0.0/16",
			allowed:          true,
		},
		{
			name:             "A non-allowed IP",
			requestIP:        "10.9.1.12",
			allowedIPNetwork: "10.0.0.0/16",
			allowed:          false,
		},
		{
			name:             "Local request",
			requestIP:        "127.0.0.1",
			allowedIPNetwork: "10.0.0.0/16",
			allowed:          true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", "/metrics", nil)
			if err != nil {
				log.Fatal(err)
			}

			os.Setenv("PROMETHEUS_IP_ALLOW_LIST", tt.allowedIPNetwork)
			req.RemoteAddr = tt.requestIP

			require.Equal(t, ipAllowed(req), tt.allowed)
		})
	}
}
