package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"net/url"
	"os"
)

var VERSION = "devel"

type PumaConfig struct {
	controlURL       string
	controlAuthToken string
	clustered        bool
}

func (pumaConfig PumaConfig) statsURL() (string, error) {
	if pumaConfig.controlURL == "" {
		return "", fmt.Errorf("invalid Puma control URL")
	}

	pumaControlURL, err := url.Parse(fmt.Sprintf("%s/stats", pumaConfig.controlURL))

	if err != nil {
		return "", fmt.Errorf("unable to parse Puma control URL: %w", err)
	}

	if pumaConfig.controlAuthToken != "" {
		pumaControlURL.RawQuery = url.Values{"token": []string{pumaConfig.controlAuthToken}}.Encode()
	}

	return pumaControlURL.String(), err
}

type Config struct {
	listen     string
	pumaConfig PumaConfig
	port       uint
	version    bool
}

func main() {
	config, output, err := parseFlags(os.Args[0], os.Args[1:])

	if errors.Is(err, flag.ErrHelp) {
		fmt.Println(output)
		os.Exit(2)
	} else if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	if config.version {
		fmt.Printf("version %s\n", VERSION)
		os.Exit(0)
	}

	server, err := NewServer(config.listen, config.port)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error creating server: %s", err)
		os.Exit(1)
	}

	server.run(&config.pumaConfig)
}

func parseFlags(progname string, args []string) (config *Config, output string, err error) {
	flags := flag.NewFlagSet(progname, flag.ContinueOnError)
	var buf bytes.Buffer
	flags.SetOutput(&buf)

	config = new(Config)

	flags.StringVar(&config.listen, "listen", "127.0.0.1", "Listen address")
	flags.UintVar(&config.port, "port", 9090, "Port")

	flags.StringVar(&config.pumaConfig.controlURL, "puma-control-url", "http://127.0.0.1:9293", "Puma control URL")
	flags.StringVar(&config.pumaConfig.controlAuthToken, "puma-control-auth-token", "", "Auth token for the Puma control server")
	flags.BoolVar(&config.pumaConfig.clustered, "puma-clustered", false, "Set to true to gather metrics when Puma works in clustered mode")
	flags.BoolVar(&config.version, "version", false, "Print version")

	err = flags.Parse(args)
	if err != nil {
		return nil, buf.String(), err
	}

	return config, buf.String(), nil
}
