package main

import (
	"errors"
	"flag"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseFlagsCorrect(t *testing.T) {
	var tests = []struct {
		args   []string
		config Config
	}{
		{[]string{},
			Config{listen: "127.0.0.1", port: 9090,
				pumaConfig: PumaConfig{controlURL: "http://127.0.0.1:9293", controlAuthToken: "", clustered: false}}},

		{[]string{"-listen", "192.168.1.1"},
			Config{listen: "192.168.1.1", port: 9090,
				pumaConfig: PumaConfig{controlURL: "http://127.0.0.1:9293", controlAuthToken: "", clustered: false}}},

		{[]string{"-listen", "192.168.1.1", "-port", "9999"},
			Config{listen: "192.168.1.1", port: 9999,
				pumaConfig: PumaConfig{controlURL: "http://127.0.0.1:9293", controlAuthToken: "", clustered: false}}},

		{[]string{"-listen", "192.168.1.1", "-port", "9999", "-puma-control-url", "http://localhost:9293"},
			Config{listen: "192.168.1.1", port: 9999,
				pumaConfig: PumaConfig{controlURL: "http://localhost:9293", controlAuthToken: "", clustered: false}}},

		{[]string{"-listen", "192.168.1.1", "-port", "9999", "-puma-control-url", "http://localhost:9293", "-puma-control-auth-token", "abc"},
			Config{listen: "192.168.1.1", port: 9999,
				pumaConfig: PumaConfig{controlURL: "http://localhost:9293", controlAuthToken: "abc", clustered: false}}},

		{[]string{"-listen", "192.168.1.1", "-port", "9999", "-puma-control-url", "http://localhost:9293", "-puma-control-auth-token", "abc", "-puma-clustered", "true"},
			Config{listen: "192.168.1.1", port: 9999,
				pumaConfig: PumaConfig{controlURL: "http://localhost:9293", controlAuthToken: "abc", clustered: true}}},
	}

	for _, tt := range tests {
		t.Run(strings.Join(tt.args, " "), func(t *testing.T) {
			conf, output, err := parseFlags("prog", tt.args)
			if err != nil {
				t.Errorf("err got %v, want nil", err)
			}
			if output != "" {
				t.Errorf("output got %q, want empty", output)
			}

			if !reflect.DeepEqual(*conf, tt.config) {
				t.Errorf("conf got %+v, want %+v", *conf, tt.config)
			}
		})
	}
}

func TestParseFlagsUsage(t *testing.T) {
	var usageArgs = []string{"-help", "-h", "--help"}

	for _, arg := range usageArgs {
		t.Run(arg, func(t *testing.T) {
			conf, output, err := parseFlags("prog", []string{arg})
			if !errors.Is(err, flag.ErrHelp) {
				t.Errorf("err got %v, want ErrHelp", err)
			}
			if conf != nil {
				t.Errorf("conf got %v, want nil", conf)
			}
			if !strings.Contains(output, "Usage of") {
				t.Errorf("output can't find \"Usage of\": %q", output)
			}
		})
	}
}

func TestParseFlagsVersion(t *testing.T) {
	var versionArgs = []string{"-version"}

	conf, output, err := parseFlags("prog", versionArgs)

	require.Nil(t, err)
	require.Equal(t, output, "")
	require.True(t, conf.version)
}

func TestPumaConfigStatsUrl(t *testing.T) {
	pumaConfig := PumaConfig{controlURL: "http:://localhost:8080", controlAuthToken: "foo"}

	pumaStatsURL, err := pumaConfig.statsURL()

	require.Nil(t, err)
	require.Equal(t, pumaStatsURL, "http:://localhost:8080/stats?token=foo")
}
