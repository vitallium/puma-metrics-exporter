package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/require"
)

func TestNewPumaCollector(t *testing.T) {
	pumaCollector := NewPumaCollector(&PumaConfig{controlURL: "http://127.0.0.1:9293", controlAuthToken: "foo"})

	require.NotNil(t, pumaCollector, "pumaCollector was not initialized")

	require.NotNil(t, pumaCollector.workers, "workerCount must not be nil")
	require.IsType(t, &prometheus.Desc{}, pumaCollector.workers, "workers must be pointer to prometheus.Desc")

	require.NotNil(t, pumaCollector.backlog, "backlog must not be nil")
	require.IsType(t, &prometheus.Desc{}, pumaCollector.backlog, "backlog must be pointer to prometheus.Desc")

	require.NotNil(t, pumaCollector.running, "running must not be nil")
	require.IsType(t, &prometheus.Desc{}, pumaCollector.running, "running must be pointer to prometheus.Desc")

	require.NotNil(t, pumaCollector.poolCapacity, "poolCapacity must not be nil")
	require.IsType(t, &prometheus.Desc{}, pumaCollector.poolCapacity, "poolCapacity must be pointer to prometheus.Desc")

	require.NotNil(t, pumaCollector.maxThreads, "maxThreads must not be nil")
	require.IsType(t, &prometheus.Desc{}, pumaCollector.maxThreads, "maxThreads must be pointer to prometheus.Desc")

	require.Nil(t, pumaCollector.bootedWorkers, "bootedWorkers must be nil")
	require.Nil(t, pumaCollector.oldWorkers, "oldWorkers must be nil")
}

func TestNewPumaCollector_InClusteredMode(t *testing.T) {
	pumaCollector := NewPumaCollector(&PumaConfig{controlURL: "http://127.0.0.1:9293", controlAuthToken: "foo", clustered: true})

	require.NotNil(t, pumaCollector, "pumaCollector was not initialized")

	require.NotNil(t, pumaCollector.workers, "workers must not be nil")
	require.IsType(t, &prometheus.Desc{}, pumaCollector.workers, "workers must not be nil")

	require.NotNil(t, pumaCollector.bootedWorkers, "bootedWorkers must not be nil")
	require.IsType(t, &prometheus.Desc{}, pumaCollector.bootedWorkers, "bootedWorkers must not be nil")

	require.NotNil(t, pumaCollector.oldWorkers, "oldWorkers must not be nil")
	require.IsType(t, &prometheus.Desc{}, pumaCollector.oldWorkers, "oldWorkers must not be nil")
}

func TestFetchPumaStats_FailedToCreateURL(t *testing.T) {
	_, err := FetchPumaStats(&PumaConfig{controlURL: "", controlAuthToken: ""})

	require.NotNil(t, err)
	require.EqualError(t, err, "failed to create Puma stats URL: invalid Puma control URL")
}

func TestFetchPumaStats_FailedToFetch(t *testing.T) {
	_, err := FetchPumaStats(&PumaConfig{controlURL: "http://localhost", controlAuthToken: "abc"})

	require.NotNil(t, err)
	require.EqualError(t, err, "failed to fetch Puma stats: Get \"http://localhost/stats?token=abc\": dial tcp [::1]:80: connect: connection refused")
}

func TestFetchPumaStats_OnErrorFromControlCenter(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.WriteHeader(503)
	}))

	defer server.Close()

	_, err := FetchPumaStats(&PumaConfig{controlURL: server.URL})

	require.NotNil(t, err)
	require.EqualError(t, err, "received an error from control center: 503 Service Unavailable")
}

func TestFetchPumaStatsFailedToDecodeResponse(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte(`{ "this is invalid json":" }`))
	}))

	defer server.Close()

	_, err := FetchPumaStats(&PumaConfig{controlURL: server.URL})

	require.NotNil(t, err)
	require.EqualError(t, err, "error decoding response from control center: unexpected EOF")
}
