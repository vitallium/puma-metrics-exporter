package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/netip"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/sync/errgroup"
)

type key int

type Server struct {
	listen       string
	port         uint
	requestIDKey key
	healthy      int32
}

func (server Server) listenAddress() string {
	return fmt.Sprintf("%s:%d", server.listen, server.port)
}

func NewServer(listen string, port uint) (*Server, error) {
	if port <= 1 || port > 65535 {
		return nil, fmt.Errorf("invalid port value: %d", port)
	}

	return &Server{listen: listen, port: port, healthy: 0, requestIDKey: 0}, nil
}

func (server Server) run(pumaConfig *PumaConfig) error {
	mainCtx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	logger := log.New(os.Stdout, "http: ", log.LstdFlags)

	registry := InitPrometheusRegistry(pumaConfig)

	router := http.NewServeMux()
	router.Handle("/metrics", promhttp.HandlerFor(registry, promhttp.HandlerOpts{}))
	router.Handle("/health", server.health())

	nextRequestID := func() string {
		return strconv.FormatInt(time.Now().UnixNano(), 10)
	}

	httpServer := &http.Server{
		Addr:         server.listenAddress(),
		Handler:      server.tracing(nextRequestID)(server.logging(logger)(router)),
		ErrorLog:     logger,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
		BaseContext: func(_ net.Listener) context.Context {
			return mainCtx
		},
	}

	g, gCtx := errgroup.WithContext(mainCtx)
	g.Go(func() error {
		atomic.StoreInt32(&server.healthy, 1)
		return httpServer.ListenAndServe()
	})
	g.Go(func() error {
		<-gCtx.Done()
		atomic.StoreInt32(&server.healthy, 0)
		return httpServer.Shutdown(context.Background())
	})

	err := g.Wait()

	if err != nil {
		logger.Println(err)
	}

	return err
}

func (server Server) health() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if atomic.LoadInt32(&server.healthy) == 1 {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusServiceUnavailable)
	})
}

func (server Server) logging(logger *log.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				requestID, ok := r.Context().Value(server.requestIDKey).(string)
				if !ok {
					requestID = "unknown"
				}
				logger.Println(requestID, r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())
			}()
			next.ServeHTTP(w, r)
		})
	}
}

func (server Server) tracing(nextRequestID func() string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if !ipAllowed(r) {
				http.Error(w, "Not Found", 404)
				return
			}

			requestID := r.Header.Get("X-Request-Id")
			if requestID == "" {
				requestID = nextRequestID()
			}
			ctx := context.WithValue(r.Context(), server.requestIDKey, requestID)
			w.Header().Set("X-Request-Id", requestID)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func ipAllowed(req *http.Request) bool {
	allowed := strings.Contains(req.RemoteAddr, "127.0.0.1")

	if !allowed {
		ips := req.Header.Get("X-Forwarded-For")
		splitIps := strings.Split(ips, ",")
		splitIps = append(splitIps, req.RemoteAddr)
		allowedIPNetwork := os.Getenv("PROMETHEUS_IP_ALLOW_LIST")
		network, _ := netip.ParsePrefix(allowedIPNetwork)

		for _, item := range splitIps {
			item := strings.Split(item, ":")[0]

			if item != "" {
				ip, _ := netip.ParseAddr(item)
				if network.Contains(ip) {
					allowed = true
					break
				}
			}
		}
	}

	return allowed
}
