package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

type PumaWorkerLastStatus struct {
	RequestsCount int   `json:"requests_count"`
	Backlog       uint8 `json:"backlog"`
	Running       uint8 `json:"running"`
	PoolCapacity  uint8 `json:"pool_capacity"`
	MaxThreads    uint8 `json:"max_threads"`
}

type WorkerStatus struct {
	StartedAt   time.Time            `json:"started_at"`
	LastCheckin time.Time            `json:"last_checkin"`
	LastStatus  PumaWorkerLastStatus `json:"last_status"`
	PID         int                  `json:"pid"`
	Index       uint8                `json:"index"`
	Phase       uint8                `json:"phase"`
	Booted      bool                 `json:"booted"`
}

type PumaStats struct {
	StartedAt     time.Time      `json:"started_at"`
	WorkerStatus  []WorkerStatus `json:"worker_status"`
	Backlog       int            `json:"backlog"`
	Running       int            `json:"running"`
	PoolCapacity  int            `json:"pool_capacity"`
	MaxThreads    int            `json:"max_threads"`
	RequestsCount int64          `json:"requests_count"`
	// available in clustered mode only
	Workers       uint8 `json:"workers"`
	Phase         uint8 `json:"phase"`
	BootedWorkers uint8 `json:"booted_workers"`
	OldWorkers    uint8 `json:"old_workers"`
}

type PumaCollector struct {
	backlog       *prometheus.Desc
	running       *prometheus.Desc
	poolCapacity  *prometheus.Desc
	maxThreads    *prometheus.Desc
	workers       *prometheus.Desc
	bootedWorkers *prometheus.Desc
	oldWorkers    *prometheus.Desc
	error         *prometheus.Desc
	pumaConfig    *PumaConfig
}

func httpClient() *http.Client {
	client := &http.Client{Timeout: 10 * time.Second}
	return client
}

func NewPumaCollector(pumaConfig *PumaConfig) *PumaCollector {
	pumaCollector := &PumaCollector{
		workers:      prometheus.NewDesc("workers", "Number of configured workers", nil, nil),
		backlog:      prometheus.NewDesc("backlog", "Number of established but unaccepted connections in the backlog", nil, nil),
		running:      prometheus.NewDesc("running", "Number of running worker threads", nil, nil),
		poolCapacity: prometheus.NewDesc("pool_capacity", "Number of allocatable worker threads", nil, nil),
		maxThreads:   prometheus.NewDesc("max_threads", "Maximum number of worker threads", nil, nil),
		error:        prometheus.NewDesc("last_scrape_error", "Whether the last scrape of metrics from Puma resulted in an error (1 for error, 0 for success)", nil, nil),
		// Puma Control Center config
		pumaConfig: pumaConfig,
	}

	if pumaConfig.clustered {
		pumaCollector.bootedWorkers = prometheus.NewDesc("booted_workers", "Number of booted workers", nil, nil)
		pumaCollector.oldWorkers = prometheus.NewDesc("old_workers", "Number of old workers", nil, nil)
	}

	return pumaCollector
}

func (collector *PumaCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- collector.workers
	ch <- collector.backlog
	ch <- collector.running
	ch <- collector.poolCapacity
	ch <- collector.maxThreads
	ch <- collector.error

	if collector.pumaConfig.clustered {
		ch <- collector.bootedWorkers
		ch <- collector.oldWorkers
	}
}

func (collector *PumaCollector) Collect(ch chan<- prometheus.Metric) {
	err := float64(0)

	pumaStats, pumaErr := FetchPumaStats(collector.pumaConfig)

	if pumaErr != nil {
		log.Printf("Error while fetching Puma stats: %s", pumaErr)
		err = 1
	} else {
		ch <- prometheus.MustNewConstMetric(collector.workers, prometheus.GaugeValue, float64(pumaStats.Workers))
		ch <- prometheus.MustNewConstMetric(collector.backlog, prometheus.GaugeValue, float64(pumaStats.Backlog))
		ch <- prometheus.MustNewConstMetric(collector.running, prometheus.GaugeValue, float64(pumaStats.Running))
		ch <- prometheus.MustNewConstMetric(collector.poolCapacity, prometheus.GaugeValue, float64(pumaStats.PoolCapacity))
		ch <- prometheus.MustNewConstMetric(collector.maxThreads, prometheus.GaugeValue, float64(pumaStats.MaxThreads))

		if collector.pumaConfig.clustered {
			ch <- prometheus.MustNewConstMetric(collector.bootedWorkers, prometheus.GaugeValue, float64(pumaStats.BootedWorkers))
			ch <- prometheus.MustNewConstMetric(collector.oldWorkers, prometheus.GaugeValue, float64(pumaStats.OldWorkers))
		}
	}

	ch <- prometheus.MustNewConstMetric(collector.error, prometheus.GaugeValue, err)
}

func InitPrometheusRegistry(pumaConfig *PumaConfig) *prometheus.Registry {
	var registry = prometheus.NewRegistry()

	registry.MustRegister(NewPumaCollector(pumaConfig))

	return registry
}

func FetchPumaStats(pumaConfig *PumaConfig) (*PumaStats, error) {
	pumaStatsURL, err := pumaConfig.statsURL()

	if err != nil {
		return nil, fmt.Errorf("failed to create Puma stats URL: %w", err)
	}

	net := httpClient()
	resp, err := net.Get(pumaStatsURL)

	if err != nil {
		return nil, fmt.Errorf("failed to fetch Puma stats: %w", err)
	}

	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Printf("Unable to close the response body stream: %s", err)
		}
	}()

	if resp.StatusCode >= 400 {
		return nil, fmt.Errorf("received an error from control center: %s", resp.Status)
	}

	var pumaStats PumaStats
	if err := json.NewDecoder(resp.Body).Decode(&pumaStats); err != nil {
		return nil, fmt.Errorf("error decoding response from control center: %w", err)
	}

	return &pumaStats, nil
}
